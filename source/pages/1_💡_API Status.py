
import streamlit as st
import requests
import json
import pandas as pd
import re

from utils import API_BASE_URL, set_page_title


set_page_title("API Status")
st.title("💡 API Status")
st.markdown('''
This page displays some information about the API and the current status.

''')

dictionaries = {
    "br": {
        "name": "Breton"
    },
    "cy": {
        "name": "Welsh"
    },
    "de": { "name": "German" },
    "es": { "name": "Spanish" },
    "fr": { "name": "French" },
    "ga": { "name": "Irish" },
    "gd": { "name": "Scottish Gaelic" },
    "kw": { "name": "Cornish" },
    "nn": { "name": "Nynorsk" }
}


try: 
    url = requests.get("https://api.opensourcedict.org/api/dictionary/info")
    text = url.text
    data = json.loads(text)
    status = True
except Exception as e:
    status = False

if status:
    first_dict = data['dictionaries'][list(data['dictionaries'].keys())[0]]
    version = first_dict['version']
    version = ".".join(re.split("[.-]", version)[0:2])

st.markdown(f"""
* **Status:** {'✅ Operational' if status else '⛔ Down'}
* **URL:** {API_BASE_URL}
{f'* **Version:** {version}' if status else ''}
""".strip())

if not status:
    st.stop()

df = pd.DataFrame(columns=[
    "Language code",
    "Langauge",
    "Repository",
    "Version",
    "Lemmas",
    "Forms"
])
df = df.set_index('Language code')

for dictionary in data['dictionaries'].values():
    name = dictionaries.get(dictionary['lang'], {}).get('name', "")
    url = dictionaries.get(dictionary['lang'], {}).get('url',
        name.lower().replace(" ", "-")
    )
    df.loc[dictionary['lang']] = [
        name,
        f"[{url}](https://gitlab.com/prvInSpace/open-{url}-dictionary)",
        dictionary['version'],
        dictionary['numberOfLemmas'],
        dictionary['numberOfForms']
    ]

#df.set_index('Language code')
df = df.sort_index()
st.subheader("Loaded dictionaries")
st.markdown(
    df.to_markdown()
)
