
import streamlit as st
import requests
import json

from endpoints import ENDPOINTS
from utils import set_page_title, fetch_api_data

set_page_title("Test endpoints")
st.title("🧪 Test API endpoints")
st.markdown('''

This page allows you to test the various endpoints of the API.
Below you'll find a list of endpoints that are available.
By changing the selection the page about that endpoint will appear.

''')

endpoint = st.selectbox("API enpoint to test", ENDPOINTS.keys())

st.subheader(f"Test {endpoint}")
st.write(f'''
Below you can test the {endpoint} end-point.
The GET parameters are provided in parenthesis where applicable. 
''')


data = fetch_api_data()

ENDPOINTS[endpoint].interface(data)

if st.button("Run query"):
    base = f"https://api.opensourcedict.org{endpoint}"
    try: 
        url = ENDPOINTS[endpoint].generate_url(base)
        st.subheader("Results")
        st.write("##### URL:")
        st.write(url)
        st.write("##### JSON response:")
        with st.spinner("Contacting the API..."):
            text = requests.get(url).text
            data = json.loads(text)
            st.json(data)
    except ValueError as e:
        st.error(f"""
        ##### Parameter error
        {e}
        """)

