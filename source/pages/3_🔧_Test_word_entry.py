
import streamlit as st
import requests
import json

from endpoints import ENDPOINTS
from utils import API_BASE_URL, fetch_api_data, set_page_title

set_page_title("Test word entry")
st.title("🧑‍🔧 Test word entry")
st.markdown('''

This page allows you to test the generated JSON from word entry JSON object.
Select the language, select the word type, enter the JSON content, and see 
the returned JSON. This uses the /api/dictionary/generate API endpoint.

''')

api_data = fetch_api_data()
lang = st.selectbox("Select language", sorted(api_data['dictionaries'].keys()))
word_type = st.selectbox("Select word type", sorted(api_data['dictionaries'][lang]['supportedTypes']))
json_source = st.text_area("JSON input", height=250, value="""{
    "normalForm": "<word>"
}
""")

if st.button("Generate JSON output"):
    try:
        json_obj = json.loads(json_source)
        if not 'normalForm' in json_obj:
            raise json.JSONDecodeError("Word needs to contain the field 'normalForm'", json_source, 0)
        url = f"{API_BASE_URL}/api/dictionary/generate?lang={lang}&type={word_type}&json={json_obj}"
        with st.spinner("Contacting the API..."):
            text = requests.get(url).text
            data = json.loads(text)
            st.json(data)
    except json.JSONDecodeError as e:
        st.error(f"##### JSON syntax error\n{e}")