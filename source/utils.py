
import streamlit as st
import requests
import json

API_BASE_URL = "https://api.opensourcedict.org"

def set_page_title(title: str):
    st.set_page_config(title, "res/logo.jpg", menu_items={
        "Get help": "https://discord.gg/UzaFmfV",
        "Report a bug": "https://gitlab.com/prvInSpace/open-source-dictionary-development/issues",
        "About":
"""#### Open Source Dictionary Development Tools
Development tools for the Open Source Dictionary project.
* **Developed by:** Contributors to the Open Source Dictionary project.
* **Maintained by:** Preben Vangberg.
* **License:** MIT
* **Source:** [Gitlab](https://gitlab.com/prvInSpace/open-source-dictionary-development)

##### Built using
"""
    })
    init_sidebar()

def init_sidebar():
    st.sidebar.markdown('''
    #### 🌍 Important links
    * [The Open Source Dictionary project](https://opensourcedict.org)
    * [Discord server](https://discord.gg/UzaFmfV)
    * [Twitter account](https://twitter.com/OpenSourceDict)
     
 
    #### 🗃️ Gitlab repositories
    * [Common Library](https://gitlab.com/prvInSpace/open-dictionary-library)
    * [API Server](https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server)
    ''')

@st.cache
def fetch_api_data():
    url = requests.get("https://api.opensourcedict.org/api/dictionary/info")
    text = url.text
    return json.loads(text)