

from abc import abstractmethod
import streamlit as st

class Endpoint():

    @abstractmethod
    def interface(self, data):
        pass

    @abstractmethod
    def generate_url(self, base):
        pass


class ApiDictionary(Endpoint):

    def interface(self, data):

        st.markdown('''
        The /api/dictionary endpoint is the main endpoint of the API and is used
        to query for words in the dictionary. There are two main ways this can be achieved:
        * Using the ID of the word
        * Searching by the word itself
        ''')

        dictionaries = list(sorted(data['dictionaries'].keys()))
        self.lang = st.selectbox("Select dictionary (lang)", dictionaries)
        self.method = st.selectbox("Search method", [ "Search by ID", "Search by word" ])
        if self.method == "Search by ID":
            self.id = st.number_input("ID of word to search for (id)", min_value=0)
        else:
            self.word = st.text_input("Word to search for (word)")
            st.markdown('''**Word type:**
                The word type is used by the API to generate results if the word is not found.
                If left empty the API will not generate results on the fly.
                This option is mutally exclusive with the extensive search option and will be ignored
                if the (search) flag is set to true.
            ''')
            self.word_type = st.selectbox("Select word type (type)",
                [ "" ] + sorted(data['dictionaries'][self.lang]['supportedTypes']))
            st.markdown('''
                **Extensive search:** By default the dictionary will only search for words based on the
                dictionary entry version of the word (i.e the lemma of the word).
                This behaviour can be overridden by setting the search flag to true.
                If this flag is set to true the API will attempt to search for words based all forms of the word.
                This flag is mutally exclusive with the (type) parameter, and the (type) parameter will be ignored if this flag is set to true. 
            ''')
            self.extensive = st.checkbox("Search by versions as well (search)")
    
    def generate_url(self, base):
        base += f"?lang={self.lang}"
        if self.method == "Search by ID":
            base += f"&id={self.id}"
        else:
            word = self.word.strip().lower()
            if not word:
                raise ValueError("Field (word) can not be empty")
            base += f"&word={word}"
            if self.extensive:
                base += f"&search={str(self.extensive).lower()}"
            elif self.word_type:
                base += f"&type={self.word_type}"
        return base

class ApiDictionaryInfo(Endpoint):

    def interface(self, data):
        st.markdown('''
        The /api/dictionary/info endpoint is used to provide information about
        what dictionaries are loaded by the API. As such it requires no parameters.
        ''')

    def generate_url(self, base):
        return base

ENDPOINTS = {
    "/api/dictionary": ApiDictionary(),
    "/api/dictionary/info": ApiDictionaryInfo()
}
