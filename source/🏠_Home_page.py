
import streamlit as st
from utils import set_page_title

set_page_title("Home page")
st.title("Open Source Dictionary Development Tools")


st.markdown('''

Welcome to the Open Source Dictionary Development page.
This page contains several tools that aid you in using the API and that aid development of the dictionaries themselves.
If you are looking for the web-interface of the dictionary itself you can find that on [opensourcedict.org](https://opensourcedict.org).

### Community

If you have any queries, questions, or just want to join the community it is recommended to join our Discord server.
You can join the server here: [Discord server](https://opensourcedict.org).

''')

