# Development Website for the Open Source Dictionary Project

Source code for the development tool website for the [Open Source Dictionary project](https://opensourcedict.org).
The website can be accessed from [dev.opensourcedict.org](https://dev.opensourcedict.org).

## Building the website

The website uses Streamlit.
Streamlit can be ran manually when developing by running `streamlit run source/🏠_Home_page.py`.
Optionally the website can be ran using a Dockerimage that is provided.
To run this image please run the following:
```bash
make build
make run
```

## Maintainers

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;

