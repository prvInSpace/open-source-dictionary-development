FROM python:3.10-slim-buster
LABEL maintainer="Preben Vangberg"

WORKDIR /code

# Install the requirements
ADD requirements.txt /code/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Add the required code
ADD source/ /code

# Expose the Streamlit port
EXPOSE 8501
ENTRYPOINT [ "streamlit", "run", "🏠_Home_page.py" ]