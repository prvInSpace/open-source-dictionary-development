

build:
	docker build -t open-source-dict-devpage .

run:
	docker run -it --restart=always \
		--name=open-source-dict-devpage\
		open-source-dict-devpage

delete:
	docker stop open-source-dict-devpage
	docker rm open-source-dict-devpage

clean: delete
	docker rmi open-source-dict-devpage
 